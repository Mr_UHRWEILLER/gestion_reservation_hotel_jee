package com.example.ApplicationReservationHotel.repository;

import com.example.ApplicationReservationHotel.model.Entities.Hotel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HotelRepository extends JpaRepository<Hotel, Long> {
}
