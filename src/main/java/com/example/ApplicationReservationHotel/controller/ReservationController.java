package com.example.ApplicationReservationHotel.controller;

import com.example.ApplicationReservationHotel.model.DTO.UtilisateurDTO;
import com.example.ApplicationReservationHotel.model.Entities.Utilisateur;
import com.example.ApplicationReservationHotel.services.IUtilisaeur;
import com.example.ApplicationReservationHotel.services.UtilisateurImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class ReservationController {
    @Autowired
    private IUtilisaeur utilisaeurService;

    @Autowired
    private UtilisateurImpl utilisateurImpl;


    @GetMapping("/page_reservation_salle_de_fete")
    public String reserversallefete(Model model) {

        return "page_reservation_salle_de_fete";
    }

    @GetMapping("/page_reservation_salle_de_conference")
    public String reserversalleconference(Model model) {

        return "page_resevation_salle_conference";
    }



    @GetMapping("/page_ajout_reservation")
    public String ajouterreservation(Model model) {

        return "page_ajout_reservation";
    }

    @GetMapping("/ajouter_hotel")
    public String ajouterHotel(Model model) {

        return "ajout_hôtel";
    }

    @GetMapping("/ajouter_utilisateur")
    public String ajouterUtilisateur(Model model) {

        return "ajout_utilisateur";
    }


    @GetMapping("/liste_hotel")
    public String listeHotel(Model model) {

        return "liste_hotels";
    }

    @GetMapping("/ajouteutilisateurform")
    public String ajouterutilisateurForm( Model model) {

        //appel de la couche service pour avoir la liste des materiels

        UtilisateurDTO utilisateurDTO = new UtilisateurDTO();

        model.addAttribute( "utilisateurDTO",utilisateurDTO);

        return "ajout_utilisateur";
    }

    @PostMapping("/enregistreutilisateur")
    public String enregistreUtilisateur(@ModelAttribute UtilisateurDTO utilisateurDTO, Model model) {

        //appel de la couche service pour avoir la liste des materiels
        utilisateurImpl.ajouterUtilisteur(utilisateurDTO);

        return "liste_utilisateurs";
    }



    @GetMapping("/liste_Utilisateur")
    public String listeUtiisateur(Model model) {

        List<UtilisateurDTO> utilisateurs = utilisateurImpl.listerUtilisateur();

        model.addAttribute("liste_Utilisateurs", utilisateurs );

        return "liste_Utilisateurs";
    }



    @GetMapping("/supprimer")
    public String supprimerUtilisateur(@RequestParam(name = "reference") int reference){

        // appel de la methode de la couche service qui supprime
        //un objet materiel en fonction de la ref

        utilisateurImpl.supprimerUtilisateur(reference);

        return "liste_Utilisateurs";
    }



}




