package com.example.ApplicationReservationHotel.services;


import com.example.ApplicationReservationHotel.model.DTO.UtilisateurDTO;
import com.example.ApplicationReservationHotel.model.Entities.Utilisateur;
import com.example.ApplicationReservationHotel.repository.UtilisateurRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
public class UtilisateurImpl implements IUtilisaeur {

    @Autowired
    private UtilisateurRepository utilisateurRepository;

    @Override
    public int ajouterUtilisteur(UtilisateurDTO utilisateurDTO) {
        Utilisateur utilisateur= new Utilisateur();
        utilisateur.setReference(utilisateurDTO.getReference());
        utilisateur.setNom(utilisateurDTO.getNom());
        utilisateur.setPrenom(utilisateurDTO.getPrenom());
        utilisateur.setDatenaissance(utilisateurDTO.getDatenaissance());
        utilisateur.setVille(utilisateurDTO.getVille());
        utilisateur.setSexe(utilisateurDTO.getSexe());
        return utilisateurRepository.save(utilisateur).getReference();

    }


    public int supprimerUtilisateur(int reference)
    {
        Utilisateur utilisateur = utilisateurRepository.findUtilisateurByReference(reference).get();

        utilisateurRepository.findUtilisateurByReference(reference).get();

        utilisateurRepository.deleteById(utilisateur.getReference());
        return 1;
//        return utilisateurRepository.deleteUtilisateurByReference(reference).intvalue();
    }

    @Override
    public int supprimerUtilisteur(int reference) {
        return 0;
    }

    @Override
    public int modifierUtilisateur(UtilisateurDTO UtilisateurDTO) {
        Utilisateur utilisateur = utilisateurRepository.findUtilisateurByReference(UtilisateurDTO.getReference()).get();

        utilisateur.setNom(UtilisateurDTO.getNom());
        utilisateur.setReference(UtilisateurDTO.getReference());
        utilisateur.setPrenom(UtilisateurDTO.getPrenom());
        utilisateur.setDatenaissance(UtilisateurDTO.getDatenaissance());

        return utilisateurRepository.save(utilisateur).getReference().intValue();
    }

    @Override
    public List<UtilisateurDTO> listerUtilisateur()
    {
        List<Utilisateur> Utilisateurs = utilisateurRepository.findAll();
        return getUtilisateurDTOS(Utilisateurs);
    }




    public UtilisateurDTO detailUtilisateur(int reference) {


        Utilisateur Utilisateur = utilisateurRepository.findUtilisateurByReference(reference).get();
        UtilisateurDTO UtilisateurDTO= new UtilisateurDTO();
        UtilisateurDTO.setReference(Utilisateur.getReference());
        UtilisateurDTO.setNom(Utilisateur.getNom());
        UtilisateurDTO.setPrenom(Utilisateur.getPrenom());
        UtilisateurDTO.setDatenaissance(Utilisateur.getDatenaissance());
        UtilisateurDTO.setVille(Utilisateur.getVille());
        UtilisateurDTO.setSexe(Utilisateur.getSexe());


        return UtilisateurDTO;
    }

    public List<UtilisateurDTO> rechercherUtilisateurs(int motcle1,String motcle2) {

        List<Utilisateur> utilisateurs = utilisateurRepository.findUtilisateurByReferenceOrNom(motcle1,motcle2);
        return getUtilisateurDTOS(utilisateurs);

    }

    private List<UtilisateurDTO> getUtilisateurDTOS(List<Utilisateur> utilisateurs) {
        List<UtilisateurDTO> utilisateurDTOS = new ArrayList<>();

        for (Utilisateur utilisateur: utilisateurs){

            UtilisateurDTO utilisateurDTO= new UtilisateurDTO();
            utilisateurDTO.setReference(utilisateur.getReference());
            utilisateurDTO.setNom(utilisateur.getNom());
            utilisateurDTO.setDatenaissance(utilisateur.getDatenaissance());
            utilisateurDTO.setPrenom(utilisateur.getPrenom());
            utilisateurDTO.setVille(utilisateur.getVille());
            utilisateurDTO.setSexe(utilisateur.getSexe());

            utilisateurDTOS.add(utilisateurDTO);

        }
        return utilisateurDTOS;
    }

}
