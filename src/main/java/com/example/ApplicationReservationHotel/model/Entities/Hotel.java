package com.example.ApplicationReservationHotel.model.Entities;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@NoArgsConstructor
@Data
@AllArgsConstructor
@Entity
public class Hotel {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long code;
    private String nom;
    private String dateCreation;
    private Long nombreEtoile;
    private String photo;
}
