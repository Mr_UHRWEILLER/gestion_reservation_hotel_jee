package com.example.ApplicationReservationHotel.repository;

import com.example.ApplicationReservationHotel.model.Entities.Utilisateur;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface  UtilisateurRepository extends JpaRepository<Utilisateur, Integer> {



    long deleteUtilisateurByReference(String reference);

    @Modifying
    @Query(value = "delete from Utilisateur m where m.reference=:reference")
    void deleteUtilisateurB(@Param("reference") String reference);



    Optional<Utilisateur> findUtilisateurByReference(int reference);

    List<Utilisateur> findUtilisateurByReferenceOrNom(int motcle1, String motcle);
}