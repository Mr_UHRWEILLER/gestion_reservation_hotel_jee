package com.example.ApplicationReservationHotel.model.DTO;

import com.example.ApplicationReservationHotel.model.Entities.Utilisateur;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UtilisateurDTO {

    private Integer reference;
    private String nom;
    private String prenom;
    private String datenaissance;
    private String ville;
    private String sexe;


}
