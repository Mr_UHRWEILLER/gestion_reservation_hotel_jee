package com.example.ApplicationReservationHotel.model.DTO;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class HotelDTO {

    private Long code;
    private String nom;
    private String dateCreation;
    private Long nombreEtoile;
    private String photo;
}
