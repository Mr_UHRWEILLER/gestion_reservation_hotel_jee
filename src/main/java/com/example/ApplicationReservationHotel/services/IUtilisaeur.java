package com.example.ApplicationReservationHotel.services;


import com.example.ApplicationReservationHotel.model.DTO.UtilisateurDTO;

import java.util.List;

public interface IUtilisaeur {

    int ajouterUtilisteur(UtilisateurDTO utilisateurDTO);
    int supprimerUtilisateur(int reference);

    int supprimerUtilisteur(int reference);

    int modifierUtilisateur(UtilisateurDTO utilisateurDTO );
    List<UtilisateurDTO> listerUtilisateur();
    UtilisateurDTO detailUtilisateur(int reference);

}
